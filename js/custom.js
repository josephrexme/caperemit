$(document).ready(function(){
  var company_deduction = 10; // in percentage
  $('#receive').val(parseFloat((100000 / 199) - ((100000 / 199) / company_deduction)).toFixed(2));
  $('#send').on('keyup', function(){
    var v = parseFloat( ($(this).val() / 199) - (($(this).val() / 199) / company_deduction) ).toFixed(2);
    $('#receive').val( Number(v) ? v : 'Invalid input');
  });
});